﻿Shader "Custom/SpecularPixelSelfShadow" {
	Properties {
		_Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_SpecColor ("Specular Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_Shininess ("Shininess", Float) = 10
	}
	SubShader {
		
		// First Pass only consists out of a dummy shader because
		// Unity doesn't allow processing of point lights in 
		// the first pass if LightMode is set to ForwardBase.
		// This only works if LightMode is set to ForwardAdd, but
		// ForwardAdd doesn't work in the first pass. If no LightMode
		// is set, pointlights do work, but position changes of the 
		// light during are not propagated to the shader anymore.  
		
		Pass {
			Tags {"LightMode" = "ForwardBase"}
			CGPROGRAM
			
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			//base input structs
			struct vertexInput {
				float4 vertex : POSITION;
			};
			
			struct vertexOutput {
				float4 pos : SV_POSITION;
			};
			
			//vertex shader
			vertexOutput vert(vertexInput input) {
				vertexOutput output;
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
			
			//fragment shader
			float4 frag(vertexOutput input) : COLOR {	
				return float4(0.0, 0.0, 0.0, 0.0);
			}
			ENDCG
		}
		
		//Second pass is set to ForwardAdd
		
		Pass {
			Tags {"LightMode" = "ForwardAdd"}
			Blend One Zero
			CGPROGRAM
			
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			//user defined variables
			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float _Shininess;
			
			//Unity defined variables
			uniform float4 _LightColor0;
			
			//Unity 3 definitions
			//float4x4 _Object2World;				//_Object2World is just another name for the current modelmatrix
			//float4x4 _World2Object;				//_World2Object is just another name for the current inverted worldmatrix
			//float4 _WorldSpaceLightPos0;
			
			//base input structs
			struct vertexInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};
			
			struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 posWorld : TEXCOORD0;
				float3 normalDir : TEXCOORD1;
			};
			
			//vertex shader
			vertexOutput vert(vertexInput input) {
				vertexOutput output;
				
				//calculate normalized normal in objectspace (because we have a directional light)
				output.posWorld = mul( _Object2World, input.vertex );
				output.normalDir = normalize( mul( float4( input.normal, 0.0 ), _World2Object ).xyz );
				
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
			
			//fragment shader
			float4 frag(vertexOutput input) : COLOR {
				float3 lightDirection;
				float atten;
				float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - input.posWorld.xyz);
				input.normalDir = normalize(input.normalDir);
				
				float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - input.posWorld.xyz;
				float distance = length(fragmentToLightSource);
				atten = 1/distance;
				lightDirection = normalize(fragmentToLightSource);
				
				//lightning
				float3 diffuseReflection = atten * _LightColor0.rgb * max( 0.0, dot(input.normalDir, lightDirection) );
				float3 specularReflection = atten * _LightColor0.rgb * _SpecColor.rgb * max( 0.0, dot(input.normalDir, lightDirection) ) * pow( max( 0.0, dot( reflect(-lightDirection, input.normalDir), viewDirection ) ), _Shininess );			
				// compute self-shadowing term
				float shadow = saturate(4* max( 0.0, dot(input.normalDir, lightDirection) ));
				
				float3 lightFinal = UNITY_LIGHTMODEL_AMBIENT.rgb + shadow * (diffuseReflection + specularReflection);
				
				return float4(lightFinal * _Color.rgb, 1.0);
			}
			
			ENDCG
		}
	}
	//fallback for release build 
	//FallBack "Diffuse"
}