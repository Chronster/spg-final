﻿Shader "Custom/DisplacementShadow" {
	Properties {
		_Color ("Color Tint", Color) = (1.0, 1.0, 1.0, 1.0)
		_MainTex ("Diffuse Texture", 2D) = "white" {}
		_NormalMap ("Normal Map", 2D) = "white" {}
		_HeightMap ("Height Map", 2D) = "white" {}
		_SpecColor ("Specular Color", Color) = (1.0, 1.0, 1.0, 1.0)
		//_Shininess ("Shininess", Float) = 10
		//_Bumpiness ("Bumpiness", Range(-10, 10)) = 1
		//_LightRange ("Light Range", Float) = 10
		//_ShadowingEnabled ("Enable Self-Shadowing", Range(0, 1)) = 1
		//_LightEnabled ("Enable Light", Range(0, 1)) = 1
		//_LinearAttenEnabled ("Enable Linear Attenuation", Range(0, 1)) = 0
		//_QuadraticAttenEnabled ("Enable Quadratic Attenuation", Range(0, 1)) = 0
		//_ConstantLinearQuadraticAttenEnabled ("Enable Constant Linear Quadratic Attenuation", Range(0, 1)) = 1
		
	}
	SubShader {
		
		// First Pass only consists out of a dummy shader because
		// Unity doesn't allow processing of point lights in 
		// the first pass if LightMode is set to ForwardBase.
		// This only works if LightMode is set to ForwardAdd, but
		// ForwardAdd doesn't work in the first pass. If no LightMode
		// is set, pointlights do work, but position changes of the 
		// light during are not propagated to the shader anymore.  
		
		Pass {
			Tags {"LightMode" = "ForwardBase"}
			CGPROGRAM
			
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 4.0 
			
			#include "UnityCG.cginc"
			
			//base input structs
			struct vertexInput {
				float4 vertex : POSITION;
			};
			
			struct vertexOutput {
				float4 pos : SV_POSITION;
			};
			
			//vertex shader
			vertexOutput vert(vertexInput input) {
				vertexOutput output;
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
			
			//fragment shader
			float4 frag(vertexOutput input) : COLOR {	
				return float4(0.0, 0.0, 0.0, 0.0);
			}
			ENDCG
		}
		
		//Second pass is set to ForwardAdd
		
		Pass {
			Tags {"LightMode" = "ForwardAdd"}
			Blend One Zero
			CGPROGRAM
			
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 4.0 
			
			#include "UnityCG.cginc"
			
			//user defined variables
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform sampler2D _NormalMap;
			uniform float4 _NormalMap_ST;
			uniform sampler2D _HeightMap;
			uniform float4 _HeightMap_ST;
			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float _Shininess;
			uniform float _Bumpiness;
			uniform float _LightRange;
			uniform float _ShadowingEnabled;
			uniform float _LightEnabled;
			uniform float _LinearAttenEnabled;
			uniform float _QuadraticAttenEnabled;
			uniform float _ConstantLinearQuadraticAttenEnabled;
			uniform float _DisplacementPower;
			uniform float _DisplacementSteps;
			uniform float _DisplacementRefinementSteps;
			
			//Unity defined variables
			uniform float4 _LightColor0;
			
			//Unity 3 definitions
			//float4x4 _Object2World;				//_Object2World is just another name for the current modelmatrix
			//float4x4 _World2Object;				//_World2Object is just another name for the current inverted worldmatrix
			//float4 _WorldSpaceLightPos0;
			
			//base input structs
			struct vertexInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
				float4 tangent : TANGENT;
			};
			
			struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
				float3 normalWorld : TEXCOORD2;
				float3 tangentWorld : TEXCOORD3;
				float3 binormalWorld : TEXCOORD4;
			};
			
			//Vertex shader
			vertexOutput vert(vertexInput input) {
				vertexOutput output;
				
				output.posWorld = mul( _Object2World, input.vertex );
								
				//output.normalWorld = normalize( mul( float4( input.normal, 0.0 ), _World2Object ).xyz );
				output.normalWorld = normalize( mul( _Object2World, float4( input.normal, 0.0 )).xyz );
				//output.tangentWorld = normalize( mul( _Object2World, input.tangent ).xyz );
				//output.normalWorld = normalize(input.normal).xyz;
				output.tangentWorld = normalize(input.tangent).xyz;
				output.binormalWorld = normalize( cross( output.normalWorld, output.tangentWorld) * input.tangent.w );
				
				output.tex = input.texcoord;
				
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
			
			//Displacement mapping function		
			float2 displaceUVCoord(float3 viewDirTangent, float2 uv, float power, float steps, float refSteps) {
				float2 displacedUV = uv.xy;
				float2 dUV = -viewDirTangent.xy * power/(steps/10.0f) * 0.08;
				float h = 1.0;
				float prev_hits = 0;
				float hit_h = 0;
				
				for (int it=0; it<steps; it++) {
					h -= (1.0f/steps);
					displacedUV += dUV;
					float h_tex = tex2D(_HeightMap, displacedUV * _HeightMap_ST.xy + _HeightMap_ST.zw).x;
					float is_first_hit = saturate((h_tex - h - prev_hits)*4999999);
					hit_h += is_first_hit * h;
					prev_hits += is_first_hit;
				}
				
				displacedUV = uv+((1.0-(hit_h+(1.0f/steps)))*dUV*steps);
				
				float2 roughDispUV = displacedUV;
				dUV *= (1.0f/refSteps);
				h = hit_h + (1.0f/steps);
				float start = hit_h + (1.0f/steps);
				prev_hits = 0;
				hit_h = 0;
				
				for (int it=0; it<refSteps; it++) {
					h -= ((1.0f/steps)/refSteps);
					displacedUV += dUV;
					float h_tex = tex2D(_HeightMap, displacedUV * _HeightMap_ST.xy + _HeightMap_ST.zw).x;
					//1 if we have a first hit, 0 else.
					float is_first_hit = saturate((h_tex - h - prev_hits)*4999999);
					hit_h += is_first_hit * h;
					prev_hits += is_first_hit;
				}
				
				return roughDispUV+((start-hit_h)*dUV*steps*refSteps);
			}
			
			//Fragment shader
			float4 frag(vertexOutput input) : COLOR {
				float3 lightDirection;
				float atten = 0;
				float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - input.posWorld.xyz);
				
				//TBN-matrix
				float3x3 World2Tex = float3x3(
					input.tangentWorld.x, input.binormalWorld.x, input.normalWorld.x,
					input.tangentWorld.y, input.binormalWorld.y, input.normalWorld.y,
					input.tangentWorld.z, input.binormalWorld.z, input.normalWorld.z
				);
				
				//Calculate displaced uv-coord
				float3 viewDirectionTangent = normalize(mul(viewDirection, World2Tex));
				input.tex.xy = displaceUVCoord(viewDirectionTangent, input.tex.xy, _DisplacementPower, _DisplacementSteps, _DisplacementRefinementSteps);
				
				//Calculate lightdirection and attenuation
				float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - input.posWorld.xyz;
				float r = length(fragmentToLightSource);
				atten = min(_LinearAttenEnabled, saturate(1-r/_LightRange));
				//atten = _LightRange/pow(r,2);
				atten += min(_QuadraticAttenEnabled, saturate(1-pow((r/_LightRange),2)));
				float sc = r;
				float sl = 0.7;
				float sq = 0.7;
				atten += min(_ConstantLinearQuadraticAttenEnabled, _LightRange/(sc + sl*r + sq * pow(r,2)));
				lightDirection = normalize(fragmentToLightSource);
				
				//Texture Maps
				float4 tex = tex2D(_MainTex, input.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);
				float4 texN = tex2D(_NormalMap, input.tex.xy * _NormalMap_ST.xy + _NormalMap_ST.zw);
				
				//Convert sampled normalmapvalue from 0..1 to a -1..1 range
				float3 sampledNormal = 2.0 * texN.rgb - float3(1.0, 1.0, 1.0);
				sampledNormal.xy = sampledNormal.xy * _Bumpiness;	//same as multiplying .z with Bumpiness, because of normalization, but here, higher bumpvalue = more bumpiness and not vice versa.
				
				//Move sampled normal from texturespace to worldspace by using Transpose of ortho TBN-Matrix
				float3x3 tex2WorldTranspose = float3x3(
					input.tangentWorld,
					input.binormalWorld,
					input.normalWorld
				);
				float3 normalDirection = normalize( mul( (sampledNormal), tex2WorldTranspose ) );
				input.normalWorld = float3( normalDirection.xy, normalDirection.z);
				
				//Lighting
				float3 diffuseReflection = atten * _LightColor0.rgb * max( 0.0, dot(input.normalWorld, lightDirection) );
				float3 specularReflection = atten * _LightColor0.rgb * _SpecColor.rgb * max( 0.0, dot(input.normalWorld, lightDirection) ) * pow( max( 0.0, dot( reflect(-lightDirection, input.normalWorld), viewDirection ) ), _Shininess );			
				//float3 diffuseReflection = atten * float3(1.0, 1.0, 1.0).rgb * max( 0.0, dot(input.normalWorld, lightDirection) );
				//float3 specularReflection = atten * float3(1.0, 1.0, 1.0).rgb * _SpecColor.rgb * max( 0.0, dot(input.normalWorld, lightDirection) ) * pow( max( 0.0, dot( reflect(-lightDirection, input.normalWorld), viewDirection ) ), _Shininess );
				
				//Self-shadowing
				//_ShadowingEnabled = ceil(max(0.0, _ShadowingEnabled)); 		//ceil should work without max, but it doesn't! Probably CG Bug
				float shadow = max( 1.0-ceil(_ShadowingEnabled), saturate(4* max( 0.0, dot(input.normalWorld, lightDirection) )));
				
				float3 lightFinal = UNITY_LIGHTMODEL_AMBIENT.rgb + ceil(max(0.0, _LightEnabled)) * shadow * (diffuseReflection + specularReflection);
				
				return float4(tex.xyz * lightFinal * _Color.rgb, 1.0);
			}
			
			ENDCG
		}
	}
	//fallback for release build 
	//FallBack "Diffuse"
}




