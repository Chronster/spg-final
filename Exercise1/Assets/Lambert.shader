﻿Shader "Custom/Lambert" {
	Properties {
		_Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
	}
	SubShader {
		Pass {
			Tags {"LightMode" = "ForwardBase"}
			CGPROGRAM
			
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			
			//user defined variables
			uniform float4 _Color;
			
			//Unity defined variables
			uniform float4 _LightColor0;
			
			//Unity 3 definitions
			//float4x4 _Object2World;
			//float4x4 _World2Object;
			//float4 _WorldSpaceLightPos0;
			
			//base input structs
			struct vertexInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};
			
			struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 col : COLOR;
			};
			
			//vertex shader
			vertexOutput vert(vertexInput input) {
				vertexOutput output;
				
				float3 normalDirection = normalize( mul( float4( input.normal, 0.0 ), _World2Object ).xyz );
				float3 lightDirection;
				float atten = 1.0;
				
				lightDirection = normalize( _WorldSpaceLightPos0.xyz );
				
				float3 diffuseReflection = atten * _LightColor0.xyz * max( 0.0, dot(normalDirection, lightDirection) );
				float3 lightFinal = diffuseReflection + UNITY_LIGHTMODEL_AMBIENT.xyz;
				
				output.col = float4(lightFinal * _Color.rgb, 1.0);
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
			
			//fragment shader
			float4 frag(vertexOutput input) : COLOR {
				return input.col;
			}
			
			ENDCG
		}
	}
	//fallback for release build 
	//FallBack "Diffuse"
}