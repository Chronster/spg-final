﻿Shader "Custom/DepthShader" {
	Properties {
		_Color ("Color Tint", Color) = (1.0, 1.0, 1.0, 1.0)
		_MainTex ("Diffuse Texture", 2D) = "white" {}
		_NormalMap ("Normal Map", 2D) = "white" {}
		_SpecColor ("Specular Color", Color) = (1.0, 1.0, 1.0, 1.0)
		
	}
	SubShader {
		
		// First Pass only consists out of a dummy shader because
		// Unity doesn't allow processing of point lights in 
		// the first pass if LightMode is set to ForwardBase.
		// This only works if LightMode is set to ForwardAdd, but
		// ForwardAdd doesn't work in the first pass. If no LightMode
		// is set, pointlights do work, but position changes of the 
		// light during are not propagated to the shader anymore.  
		
		Pass {
			Tags {"LightMode" = "ForwardBase"}
			CGPROGRAM
			
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0 
			
			#include "UnityCG.cginc"
			
			//base input structs
			struct vertexInput {
				float4 vertex : POSITION;
			};
			
			struct vertexOutput {
				float4 pos : SV_POSITION;
			};
			
			//vertex shader
			vertexOutput vert(vertexInput input) {
				vertexOutput output;
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
			
			//fragment shader
			float4 frag(vertexOutput input) : COLOR {	
				return float4(0.0, 0.0, 0.0, 0.0);
			}
			ENDCG
		}
		
		//Second pass is set to ForwardAdd
		
		Pass {
			Tags {"LightMode" = "ForwardAdd"}
			Blend One Zero
			CGPROGRAM
			
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 5.0 
			
			#include "UnityCG.cginc"
			
			//user defined variables
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform sampler2D _NormalMap;
			uniform float4 _NormalMap_ST;
			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float _Shininess;
			uniform float _Bumpiness;
			uniform float _LightRange;
			uniform float _ShadowingEnabled;
			uniform float _LightEnabled;
			uniform float _LinearAttenEnabled;
			uniform float _QuadraticAttenEnabled;
			uniform float _ConstantLinearQuadraticAttenEnabled;
			uniform sampler2D _DepthTexture;
			uniform float4 _DepthTexture_ST;
			uniform float4x4 _DepthMatrix;
			uniform float4 _LightPos;
			uniform float4 _DepthTextureOffset;		//offset for PCF
			uniform float _IsPcfEnabled;
			uniform float _Bias;					//shadow bias
			
			//Unity defined variables
			uniform float4 _LightColor0;
			
			//base input structs
			struct vertexInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
				float4 tangent : TANGENT;
			};
			
			struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
				float3 normalWorld : TEXCOORD2;
				float3 tangentWorld : TEXCOORD3;
				float3 binormalWorld : TEXCOORD4;
				float4 posLight : TEXCOORD5;	//Pos as viewed from light source
			};
			
			//Vertex shader
			vertexOutput vert(vertexInput input) {
				vertexOutput output;
				
				//Calculate normalized normal in objectspace (because we have a directional light)
				output.posWorld = mul( _Object2World, input.vertex );
				output.normalWorld = normalize( mul( float4( input.normal, 0.0 ), _World2Object ).xyz );
				output.tangentWorld = normalize( mul( _Object2World, input.tangent ).xyz );
				output.binormalWorld = normalize( cross( output.normalWorld, output.tangentWorld) * input.tangent.w );
				output.tex = input.texcoord;
				output.posLight = mul( _Object2World, input.vertex );
				output.posLight = mul( _DepthMatrix, output.posLight );
				
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
			
			//Fragment shader
			float4 frag(vertexOutput input) : COLOR {
				float3 lightDirection;
				float atten = 0;
				float2 projectTexCoord;
				float depthValue = 0.0;
				float3 diffuseReflection = float3(0.0, 0.0, 0.0);;
				float3 specularReflection = float3(0.0, 0.0, 0.0);;
				float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - input.posWorld.xyz);
				input.normalWorld = normalize(input.normalWorld);
				
				float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - input.posWorld.xyz;
				float r = length(fragmentToLightSource);
				atten = min(_LinearAttenEnabled, saturate(1-r/_LightRange));
				//atten = _LightRange/pow(r,2);
				atten += min(_QuadraticAttenEnabled, saturate(1-pow((r/_LightRange),2)));
				float sc = r;
				float sl = 0.7;
				float sq = 0.7;
				atten += min(_ConstantLinearQuadraticAttenEnabled, _LightRange/(sc + sl*r + sq * pow(r,2)));
				lightDirection = normalize(fragmentToLightSource);
				
				//Texture Maps
				float4 tex = tex2D(_MainTex, input.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);
				float4 texN = tex2D(_NormalMap, input.tex.xy * _NormalMap_ST.xy + _NormalMap_ST.zw);
				
				//Calculate projected texture coordinates for depth texture lookup
    			projectTexCoord.x = input.posLight.x / input.posLight.w;
    			projectTexCoord.y = input.posLight.y / input.posLight.w;
				
				// Determine if the projected coordinates are in the 0 to 1 range.  If so then this pixel is in the view of the light.
    			if((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
    			//if(saturate(projectTexCoord.x) == projectTexCoord.x)
    			{
    				
    				// Calculate the depth of the light.
        			float lightDepthValue = input.posLight.z / input.posLight.w;
        			
        			// Subtract the bias from the lightDepthValue.
        			lightDepthValue = lightDepthValue - _Bias;
        			
    				// For softshadows, sample the shadow map depth value from the depth texture 16 times, using the sampler at the projected texture coordinate location.
        			for (int i=0; i<4; i++) {
        				for (int j=0; j<4; j++) {
        					// Take 16 samples and test (without using if) wether texel is in shadow or not. Then accumulate testresults.
        					float2 offset = float2(_DepthTextureOffset[i], _DepthTextureOffset[j]);
        					float tmpDepthValue = tex2D(_DepthTexture, projectTexCoord.xy + offset + _DepthTexture_ST.zw).r;	
        					depthValue += saturate((tmpDepthValue - lightDepthValue) * 999999); //multiply by high number to receive only either 0 or 1 and nothing in between	
        				}
        			}
        			// Divide accumulated testresults by nr. of samples
        			depthValue /= 16;
        			
        			// For hard shadows, sample only once
        			float depthValueHard = tex2D(_DepthTexture, projectTexCoord.xy * _DepthTexture_ST.xy + _DepthTexture_ST.zw).r;
        			depthValueHard = saturate((depthValueHard - lightDepthValue) * 999999); //multiply by high number to receive only either 0 or 1 and nothing in between
        			
        			// Determine wether hard- or softshadows are switched on and take correct value
        			depthValue = _IsPcfEnabled * depthValue + (1.0-_IsPcfEnabled) * depthValueHard;
					
					//Convert sampled normalmapvalue from 0..1 to a -1..1 range
					float3 sampledNormal = 2.0 * texN.rgb - float3(1.0, 1.0, 1.0);
					sampledNormal.xy = sampledNormal.xy * _Bumpiness;	//same as multiplying .z with Bumpiness, because of normalization, but here, higher bumpvalue = more bumpiness and not vice versa.
				
					//Move sampled normal from texturespace to worldspace by using tangentspace
					float3x3 tex2WorldTranspose = float3x3(
						input.tangentWorld,
						input.binormalWorld,
						input.normalWorld
					);
					float3 normalDirection = normalize( mul( (sampledNormal), tex2WorldTranspose ) );
					//input.normalWorld = float3( normalDirection.xy, normalDirection.z);
				
					//Lighting: depthValue is 0 or 1 for hard shadows and something inbetween 0..1 for soft shadows
					diffuseReflection = depthValue * atten * _LightColor0.rgb * max( 0.0, dot(input.normalWorld, lightDirection) );
					specularReflection = depthValue * atten * _LightColor0.rgb * _SpecColor.rgb * max( 0.0, dot(input.normalWorld, lightDirection) ) * pow( max( 0.0, dot( reflect(-lightDirection, input.normalWorld), viewDirection ) ), _Shininess );							
				}
				//Self-shadowing
				float shadow = max( 1.0-ceil(_ShadowingEnabled), saturate(4* max( 0.0, dot(input.normalWorld, lightDirection) )));
				
				float3 lightFinal = UNITY_LIGHTMODEL_AMBIENT.rgb + ceil(max(0.0, _LightEnabled)) * shadow * (diffuseReflection + specularReflection);
				
				return float4(tex.xyz * lightFinal * _Color.rgb, 1.0);
			}
			
			ENDCG
		}
	}
	//fallback for release build 
	//FallBack "Diffuse"
}
