﻿using UnityEngine;
using System.Collections;

public class UserInterface : MonoBehaviour {

	private bool toggleLight = true;
	private bool toggleSelfShadow = true;
	private float shininess = 10.0f;
	private float bumpiness = 1.0f;
	private bool isInverted = false;
	private float lightRange = 100.0f;
	private string bumpinessStr;
	private int attenSelection;
	private float displacementPower = 0.04f;
	private float displacementSteps = 10;
	private float displacementRefinementSteps = 5;
	private bool togglePCF = true;
	private float bias = 0.005f;
	//public Material material;
	
	void OnGUI () {
		// Create background box
		GUI.Box(new Rect(10,10,180,555), "Shader Configuration");

		if (isInverted == true) {
			bumpinessStr = bumpiness.ToString("-0.00");
		} else {
			bumpinessStr = bumpiness.ToString("0.00");
		}

		// Create GUI elements
		//GUI.Label (new Rect (15, 30, 170, 20), "------------------------------------------------");
		GUI.Box(new Rect(15,35,170,50), "");
		toggleLight = GUI.Toggle (new Rect (25, 40, 150, 20), toggleLight, " Lights Enabled");
		toggleSelfShadow = GUI.Toggle (new Rect (25, 60, 150, 20), toggleSelfShadow, " Selfshadow Enabled");
		GUI.Box(new Rect(15,90,170,50), "");
		GUI.Label (new Rect (25, 95, 150, 20), "Shininess:  " + shininess.ToString("0.00"));
		shininess = GUI.HorizontalSlider (new Rect (25, 120, 150, 20), shininess, 0.0f, 20.0f);
		GUI.Box(new Rect(15,145,170,70), "");
		GUI.Label (new Rect (25, 150, 150, 20), "Bumpiness:  " + bumpinessStr);
		bumpiness = GUI.HorizontalSlider (new Rect (25, 175, 150, 20), bumpiness, 0.0f, 10.0f);
		isInverted = GUI.Toggle (new Rect (25, 190, 150, 20), isInverted, " Invert");
		GUI.Box(new Rect(15,220,170,50), "");
		GUI.Label (new Rect (25, 225, 150, 25), "Light Range:  " + lightRange.ToString("0.00"));
		lightRange = GUI.HorizontalSlider (new Rect (25, 250, 150, 20), lightRange, 0.01f, 100.0f);
		GUI.Box(new Rect(15,275,170,70), "");
		string[] radioBtnStrings = {" Linear Attenuation", " Quadratic Attenuation", " CLQ Attenuation"};
		attenSelection = GUI.SelectionGrid(new Rect(25, 280, 170, 55), attenSelection, radioBtnStrings, 1, "toggle");
		GUI.Box(new Rect(15,350,170,135), "");
		GUI.Label (new Rect (25, 355, 150, 25), "Displ. Power:  " + displacementPower.ToString("0.00"));
		displacementPower = GUI.HorizontalSlider (new Rect (25, 380, 150, 20), displacementPower, 0.00f, 0.25f);
		GUI.Label (new Rect (25, 395, 150, 25), "Displ. Steps:  " + displacementSteps.ToString());
		displacementSteps = GUI.HorizontalSlider (new Rect (25, 420, 150, 20), displacementSteps, 1, 40);
		displacementSteps = (int)displacementSteps;
		GUI.Label (new Rect (25, 440, 150, 25), "Displ. Ref. Steps:  " + displacementRefinementSteps.ToString());
		displacementRefinementSteps = GUI.HorizontalSlider (new Rect (25, 465, 150, 20), displacementRefinementSteps, 1, 40);
		displacementRefinementSteps = (int)displacementRefinementSteps;
		GUI.Box(new Rect(15,490,170,70), "");
		togglePCF = GUI.Toggle (new Rect (25, 492, 150, 20), togglePCF, "Soft-Shadowing");
		GUI.Label (new Rect (25, 515, 150, 20), "Shadow Bias:  " + bias.ToString("0.000"));
		bias = GUI.HorizontalSlider (new Rect (25, 540, 150, 20), bias, 0.001f, 0.1f);


		// Set values in material
		if (isInverted == true) {
			Shader.SetGlobalFloat ("_Bumpiness", bumpiness * (-1));
		} else {
			Shader.SetGlobalFloat ("_Bumpiness", bumpiness);
		}
		if (toggleLight == true) {
			Shader.SetGlobalFloat ("_LightEnabled", 1.0f);
		} else {
			Shader.SetGlobalFloat ("_LightEnabled", 0.0f);
		}

		if (togglePCF == true) {
			Shader.SetGlobalFloat ("_IsPcfEnabled", 1.0f);
		} else {
			Shader.SetGlobalFloat ("_IsPcfEnabled", 0.0f);
		}

		if (toggleSelfShadow == true) {
			Shader.SetGlobalFloat ("_ShadowingEnabled", 1.0f);
		} else {
			Shader.SetGlobalFloat ("_ShadowingEnabled", 0.0f);
		}

		switch (attenSelection) {
		  case 0:
			Shader.SetGlobalFloat ("_LinearAttenEnabled", 1.0f);
			Shader.SetGlobalFloat ("_QuadraticAttenEnabled", 0.0f);
			Shader.SetGlobalFloat ("_ConstantLinearQuadraticAttenEnabled", 0.0f);
			break;
		  case 1:
			Shader.SetGlobalFloat ("_LinearAttenEnabled", 0.0f);
			Shader.SetGlobalFloat ("_QuadraticAttenEnabled", 1.0f);
			Shader.SetGlobalFloat ("_ConstantLinearQuadraticAttenEnabled", 0.0f);
			break;
		  case 2:
			Shader.SetGlobalFloat ("_LinearAttenEnabled", 0.0f);
			Shader.SetGlobalFloat ("_QuadraticAttenEnabled", 0.0f);
			Shader.SetGlobalFloat ("_ConstantLinearQuadraticAttenEnabled", 1.0f);
			break;
		  default:
			Shader.SetGlobalFloat ("_LinearAttenEnabled", 1.0f);
			Shader.SetGlobalFloat ("_QuadraticAttenEnabled", 0.0f);
			Shader.SetGlobalFloat ("_ConstantLinearQuadraticAttenEnabled", 0.0f);
			break;
		}
				
		Shader.SetGlobalFloat ("_Shininess", shininess);
		Shader.SetGlobalFloat ("_LightRange", lightRange);
		Shader.SetGlobalFloat ("_DisplacementPower", displacementPower);
		Shader.SetGlobalFloat ("_DisplacementSteps", displacementSteps);
		Shader.SetGlobalFloat ("_DisplacementRefinementSteps", displacementRefinementSteps);
		Shader.SetGlobalFloat ("_Bias", bias);
	}
}
