﻿Shader "Custom/SpecularTexture" {
	Properties {
		_Color ("Color Tint", Color) = (1.0, 1.0, 1.0, 1.0)
		_MainTex ("Diffuse Texture", 2D) = "white" {}
		_NormalMap ("Normal Map", 2D) = "white" {}
		_SpecColor ("Specular Color", Color) = (1.0, 1.0, 1.0, 1.0)
		//_Shininess ("Shininess", Float) = 10
		//_Bumpiness ("Bumpiness", Range(-10, 10)) = 1
		//_LightRange ("Light Range", Float) = 10
		//_ShadowingEnabled ("Enable Self-Shadowing", Range(0, 1)) = 1
		//_LightEnabled ("Enable Light", Range(0, 1)) = 1
		//_LinearAttenEnabled ("Enable Linear Attenuation", Range(0, 1)) = 0
		//_QuadraticAttenEnabled ("Enable Quadratic Attenuation", Range(0, 1)) = 0
		//_ConstantLinearQuadraticAttenEnabled ("Enable Constant Linear Quadratic Attenuation", Range(0, 1)) = 1
		
	}
	SubShader {
		
		// First Pass only consists out of a dummy shader because
		// Unity doesn't allow processing of point lights in 
		// the first pass if LightMode is set to ForwardBase.
		// This only works if LightMode is set to ForwardAdd, but
		// ForwardAdd doesn't work in the first pass. If no LightMode
		// is set, pointlights do work, but position changes of the 
		// light during are not propagated to the shader anymore.  
		
		Pass {
			Tags {"LightMode" = "ForwardBase"}
			CGPROGRAM
			
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0 
			
			#include "UnityCG.cginc"
			
			//base input structs
			struct vertexInput {
				float4 vertex : POSITION;
			};
			
			struct vertexOutput {
				float4 pos : SV_POSITION;
			};
			
			//vertex shader
			vertexOutput vert(vertexInput input) {
				vertexOutput output;
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
			
			//fragment shader
			float4 frag(vertexOutput input) : COLOR {	
				return float4(0.0, 0.0, 0.0, 0.0);
			}
			ENDCG
		}
		
		//Second pass is set to ForwardAdd
		
		Pass {
			Tags {"LightMode" = "ForwardAdd"}
			Blend One Zero
			CGPROGRAM
			
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0 
			
			#include "UnityCG.cginc"
			
			//user defined variables
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform sampler2D _NormalMap;
			uniform float4 _NormalMap_ST;
			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float _Shininess;
			uniform float _Bumpiness;
			uniform float _LightRange;
			uniform float _ShadowingEnabled;
			uniform float _LightEnabled;
			uniform float _LinearAttenEnabled;
			uniform float _QuadraticAttenEnabled;
			uniform float _ConstantLinearQuadraticAttenEnabled;
			
			//Unity defined variables
			uniform float4 _LightColor0;
			
			//Unity 3 definitions
			//float4x4 _Object2World;				//_Object2World is just another name for the current modelmatrix
			//float4x4 _World2Object;				//_World2Object is just another name for the current inverted worldmatrix
			//float4 _WorldSpaceLightPos0;
			
			//base input structs
			struct vertexInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
				float4 tangent : TANGENT;
			};
			
			struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
				float3 normalWorld : TEXCOORD2;
				float3 tangentWorld : TEXCOORD3;
				float3 binormalWorld : TEXCOORD4;
			};
			
			//Vertex shader
			vertexOutput vert(vertexInput input) {
				vertexOutput output;
				
				//Calculate normalized normal in objectspace (because we have a directional light)
				output.posWorld = mul( _Object2World, input.vertex );
				output.normalWorld = normalize( mul( float4( input.normal, 0.0 ), _World2Object ).xyz );
				output.tangentWorld = normalize( mul( _Object2World, input.tangent ).xyz );
				output.binormalWorld = normalize( cross( output.normalWorld, output.tangentWorld) * input.tangent.w );
				output.tex = input.texcoord;
				
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
			
			//Fragment shader
			float4 frag(vertexOutput input) : COLOR {
				float3 lightDirection;
				float atten = 0;
				float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - input.posWorld.xyz);
				input.normalWorld = normalize(input.normalWorld);
				
				float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - input.posWorld.xyz;
				float r = length(fragmentToLightSource);
				atten = min(_LinearAttenEnabled, saturate(1-r/_LightRange));
				//atten = _LightRange/pow(r,2);
				atten += min(_QuadraticAttenEnabled, saturate(1-pow((r/_LightRange),2)));
				float sc = r;
				float sl = 0.7;
				float sq = 0.7;
				atten += min(_ConstantLinearQuadraticAttenEnabled, _LightRange/(sc + sl*r + sq * pow(r,2)));
				lightDirection = normalize(fragmentToLightSource);
				
				//Texture Maps
				float4 tex = tex2D(_MainTex, input.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);
				float4 texN = tex2D(_NormalMap, input.tex.xy * _NormalMap_ST.xy + _NormalMap_ST.zw);
				
				//Convert sampled normalmapvalue from 0..1 to a -1..1 range
				float3 sampledNormal = 2.0 * texN.rgb - float3(1.0, 1.0, 1.0);
				sampledNormal.xy = sampledNormal.xy * _Bumpiness;	//same as multiplying .z with Bumpiness, because of normalization, but here, higher bumpvalue = more bumpiness and not vice versa.
				
				//Move sampled normal from texturespace to worldspace by using tangentspace
				float3x3 tex2WorldTranspose = float3x3(
					input.tangentWorld,
					input.binormalWorld,
					input.normalWorld
				);
				float3 normalDirection = normalize( mul( (sampledNormal), tex2WorldTranspose ) );
				input.normalWorld = float3( normalDirection.xy, normalDirection.z);
				
				//Lighting
				float3 diffuseReflection = atten * _LightColor0.rgb * max( 0.0, dot(input.normalWorld, lightDirection) );
				float3 specularReflection = atten * _LightColor0.rgb * _SpecColor.rgb * max( 0.0, dot(input.normalWorld, lightDirection) ) * pow( max( 0.0, dot( reflect(-lightDirection, input.normalWorld), viewDirection ) ), _Shininess );			
				//float3 diffuseReflection = atten * float3(1.0, 1.0, 1.0).rgb * max( 0.0, dot(input.normalWorld, lightDirection) );
				//float3 specularReflection = atten * float3(1.0, 1.0, 1.0).rgb * _SpecColor.rgb * max( 0.0, dot(input.normalWorld, lightDirection) ) * pow( max( 0.0, dot( reflect(-lightDirection, input.normalWorld), viewDirection ) ), _Shininess );
				
				//Self-shadowing
				//_ShadowingEnabled = ceil(max(0.0, _ShadowingEnabled)); 		//ceil should work without max, but it doesn't! Probably CG Bug
				float shadow = max( 1.0-ceil(_ShadowingEnabled), saturate(4* max( 0.0, dot(input.normalWorld, lightDirection) )));
				
				float3 lightFinal = UNITY_LIGHTMODEL_AMBIENT.rgb + ceil(max(0.0, _LightEnabled)) * shadow * (diffuseReflection + specularReflection);
				
				return float4(tex.xyz * lightFinal * _Color.rgb, 1.0);
			}
			
			ENDCG
		}
	}
	//fallback for release build 
	//FallBack "Diffuse"
}
