﻿using UnityEngine;
using System.Collections;

public class CreatePointCloud : MonoBehaviour {
	public Texture2D texture1;
	public Texture2D texture2;

	void createFireEffect(Vector3 emitter){
		
		int vertexCount = 400;
		//float[] lifeTime = new float[vertexCount];
		//float[] speed = new float[vertexCount];
		float startTime = new float();
		Vector3[] vertices = new Vector3[vertexCount];
		Vector4[] customVertexAttributes = new Vector4[vertexCount];
		int[] indices = new int[vertexCount];

		startTime = Time.time;
		Vector2 randomCirclePos;
		
		for(int i=0; i<vertexCount; i++){
			randomCirclePos = Random.insideUnitCircle * 1;
			//vertices[i] = new Vector3(emitter.x + randomCirclePos.x, emitter.y, emitter.z + randomCirclePos.y);
			vertices[i] = new Vector3(randomCirclePos.x, 0.0f, randomCirclePos.y);
			indices[i] = i;
			//speed
			customVertexAttributes[i].x = Random.Range(1.0f, 2.0f);
			//particle start delay
			customVertexAttributes[i].y = Random.Range(0.0f, 10.0f);
			//life time
			customVertexAttributes[i].z = Random.Range(9.0f, 10.0f);
			//smoke speed horizontal
			customVertexAttributes[i].w = Random.Range(0.5f, 0.8f);
		}



		GameObject plane = new GameObject("Plane");
		MeshFilter mf = (MeshFilter)plane.AddComponent(typeof(MeshFilter));

		Mesh m = new Mesh();
		m.vertices = vertices;
		m.SetIndices(indices, MeshTopology.Points, 0);
		m.tangents = customVertexAttributes;
		m.RecalculateBounds();
		
		//MeshFilter mf = GetComponent<MeshFilter>();
		mf.mesh = m;

		MeshRenderer renderer = plane.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
		renderer.material.shader = Shader.Find ("Custom/particleShader");
		renderer.material.color = new Color(0.8f, 0.1f, 0.0f, 1.0f); 
		renderer.material.SetFloat ("_StartTime", startTime);
		renderer.material.SetTexture ("_SpriteTex", texture1);
		renderer.material.SetTexture ("_SpriteTex2", texture2);
		plane.transform.position = new Vector3 (emitter.x, emitter.y, emitter.z);
		Debug.Log(m.bounds);
	}

	void Update() {
		if (Input.GetMouseButtonUp (2)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, 100)) {
				Debug.Log ("hit" + hit.point.ToString());
				createFireEffect(hit.point);

			}
		}
	}
}
