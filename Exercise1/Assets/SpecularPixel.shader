﻿Shader "Custom/SpecularPixel" {
	Properties {
		_Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_SpecColor ("Specular Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_Shininess ("Shininess", Float) = 10
	}
	SubShader {
		Pass {
			Tags {"LightMode" = "ForwardBase"}
			CGPROGRAM
			
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			
			//user defined variables
			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float _Shininess;
			
			//Unity defined variables
			uniform float4 _LightColor0;
			
			//Unity 3 definitions
			//float4x4 _Object2World;				//_Object2World is just another name for the current modelmatrix
			//float4x4 _World2Object;				//_World2Object is just another name for the current inverted worldmatrix
			//float4 _WorldSpaceLightPos0;
			
			//base input structs
			struct vertexInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};
			
			struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 posWorld : TEXCOORD0;
				float3 normalDir : TEXCOORD1;
			};
			
			//vertex shader
			vertexOutput vert(vertexInput input) {
				vertexOutput output;
				
				//calculate normalized normal in objectspace (because we have a directional light)
				output.normalDir = normalize( mul( float4( input.normal, 0.0 ), _World2Object ).xyz );
				//calculate object world position by multiplying local vertex position with current model matrix
				output.posWorld = mul( input.vertex, _Object2World );
				
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
			
			//fragment shader
			float4 frag(vertexOutput input) : COLOR {
				float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - input.posWorld.xyz);
				// Because we have a directional light, this is indeed the local light direction vector from 0,0,0 (object center) to lightsource
				float3 lightDirection = normalize( _WorldSpaceLightPos0.xyz );
				float atten = 1.0;
				
				//float atten;
				//float lightDirection;
				//float3 lightSource = (6.0, 3.0, -6.0);
				//float3 fragmentToLightSource = lightSource - input.posWorld.xyz;
				//float distance = length(fragmentToLightSource);
				//atten = 1/distance;
				//atten = 1;
				//lightDirection = normalize(fragmentToLightSource);	
				//float3 diffuseReflection = atten * _LightColor0.rgb * max( 0.0, dot(input.normalDir, lightDirection) );
				//return float4(_LightColor0.rgb, 1.0);			
				
				//lightning
				float3 diffuseReflection = atten * _LightColor0.rgb * max( 0.0, dot(input.normalDir, lightDirection) );
				float3 specularReflection = atten * _LightColor0.rgb * _SpecColor.rgb * max( 0.0, dot(input.normalDir, lightDirection) ) * pow( max( 0.0, dot( reflect(-lightDirection, input.normalDir), viewDirection ) ), _Shininess );			
				//float3 specularReflection = atten * _LightColor0.rgb * _SpecColor.rgb * pow( max( 0.0, dot( reflect(-lightDirection, input.normalDir), viewDirection ) ), _Shininess );
				
				float3 lightFinal = diffuseReflection + specularReflection + UNITY_LIGHTMODEL_AMBIENT;
				
				return float4(lightFinal * _Color.rgb, 1.0);
			}
			
			ENDCG
		}
	}
	//fallback for release build 
	//FallBack "Diffuse"
}