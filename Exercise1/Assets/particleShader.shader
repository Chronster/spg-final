﻿Shader "Custom/particleShader" {
	Properties {
		_Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_StartTime ("Start time", float) = 0.0
		_SpriteTex ("Base (RGB)", 2D) = "white" {}
		_SpriteTex2 ("Smoke (RGB)", 2D) = "white" {}
		_Size ("Billboard Size", float) = 1.0
	}
	SubShader {
		Blend SrcAlpha One
    	AlphaTest Greater .01
    	ColorMask RGB
		Cull Off Lighting Off ZWrite Off Fog { Color (0,0,0,0) }
		Tags {"Queue" = "Transparent" }
		Pass {
			LOD 200
			CGPROGRAM
			
			//pragmas
			#pragma target 5.0
			#pragma vertex vert
			#pragma fragment frag
			#pragma geometry GS_Main
			#include "UnityCG.cginc"
			
			//user defined variables
			uniform float4 _Color;
			uniform float _StartTime;
			uniform float _Size;
			uniform Texture2D _SpriteTex;
			uniform Texture2D _SpriteTex2;
			uniform SamplerState sampler_SpriteTex;
			
			//base input structs
			struct vertexInput {
				float4 vertex : POSITION;
				float4 tangent : TANGENT;
				float2 tex0	: TEXCOORD0;
			};
			
			struct gsInput {
				float4 pos : POSITION;
				float4 col : COLOR;
				float2 tex0	: TEXCOORD0;
			};
			
			struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 col : COLOR;
				float2 tex0	: TEXCOORD0;
			};
			
			//vertex shader
			gsInput vert(vertexInput input) {
				gsInput output;
				float lifeTime = input.tangent.z;
				float startDelay = input.tangent.y;
				float speed = input.tangent.x;
				//float deltaTime = modf(max(0.0, _Time.y - _StartTime),10);
				float deltaTime = max(0.0, _Time.y - _StartTime);
				deltaTime = max(0.0, deltaTime - startDelay);
				deltaTime = fmod(deltaTime, lifeTime);
				
				if (deltaTime/lifeTime < 0.5) {
					output.col = float4(_Color.rgb, 1.0-saturate(deltaTime/(lifeTime/2)));
					// Fade-in / fade-out
					output.col.a = saturate(pow(min(output.col.a, deltaTime),2.0));
					// Decrease billboardsize over time
					output.col.b = 1.0 - (deltaTime/(lifeTime/2));
					input.vertex.y = deltaTime * speed;
					output.col.g = 0.0;
					output.col.r = 1.0;
				
				} else {
					output.col = float4(_Color.rgb, 1.0-saturate(deltaTime/lifeTime));
					// Fade-in / fade-out
					output.col.a = saturate(pow(min(output.col.a, deltaTime),2.0));
					// Increase billboardsize over time
					output.col.b = (deltaTime/lifeTime)*2.0;
					input.vertex.y = (lifeTime/2.0) * speed + (deltaTime-(lifeTime/2.0)) * speed * 0.2;
					input.vertex.xz *= (deltaTime-(lifeTime/2.0)) * input.tangent.w * (input.vertex.y * 0.3);  
					output.col.g = 1.0;
					output.col.r = 0.0;
				}

				output.pos =  mul(_Object2World, input.vertex);
				output.tex0 = float2(0, 0);
				return output;
			}
			
			// Geometry Shader
			[maxvertexcount(4)]
			void GS_Main(point gsInput p[1], inout TriangleStream<vertexOutput> triStream) {
				float3 up = float3(0, 1, 0);
				float3 look = _WorldSpaceCameraPos - p[0].pos;
				look.y = 0;
				look = normalize(look);
				float3 right = cross(up, look);
				
				float size = max(p[0].col.b, 0.3)*2.0;	
				float halfS = 0.5f * size;
				//float halfS = 0.5f;
							
				float4 v[4];
				v[0] = float4(p[0].pos + halfS * right - halfS * up, 1.0f);
				v[1] = float4(p[0].pos + halfS * right + halfS * up, 1.0f);
				v[2] = float4(p[0].pos - halfS * right - halfS * up, 1.0f);
				v[3] = float4(p[0].pos - halfS * right + halfS * up, 1.0f);

				float4x4 vp = mul(UNITY_MATRIX_MVP, _World2Object);
				vertexOutput pIn;
				pIn.pos = mul(vp, v[0]);
				pIn.tex0 = float2(1.0f, 0.0f);
				pIn.col = p[0].col;
				triStream.Append(pIn);

				pIn.pos =  mul(vp, v[1]);
				pIn.tex0 = float2(1.0f, 1.0f);
				pIn.col = p[0].col;
				triStream.Append(pIn);

				pIn.pos =  mul(vp, v[2]);
				pIn.tex0 = float2(0.0f, 0.0f);
				pIn.col = p[0].col;
				triStream.Append(pIn);

				pIn.pos =  mul(vp, v[3]);
				pIn.tex0 = float2(0.0f, 1.0f);
				pIn.col = p[0].col;
				triStream.Append(pIn);
			}
			
			//fragment shader
			float4 frag(vertexOutput input) : COLOR {
				//return _Color;
				//return input.col;
				float4 finalColor = _SpriteTex.Sample(sampler_SpriteTex, input.tex0) * input.col.r + _SpriteTex2.Sample(sampler_SpriteTex, input.tex0) * input.col.g;
				finalColor.a = min(finalColor.a, input.col.a);
				return finalColor;
			}
			
			ENDCG
		}
	}
	//fallback for release build 
	//FallBack "Diffuse"
}

