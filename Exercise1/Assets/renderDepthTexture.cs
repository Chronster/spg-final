﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]

public class renderDepthTexture : MonoBehaviour {
	private RenderTexture _depthNormalTex;
	private GameObject _depthNormalCam;
	public RenderTexture _outputTexture;
	public Material mat;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void CleanUpTextures() {
		if (_depthNormalTex) {
			RenderTexture.ReleaseTemporary(_depthNormalTex);
			_depthNormalTex = null;
		}
	}		
	
	void OnPreRender () 
	{
		if (!enabled || !gameObject.activeSelf) return;
		
		CleanUpTextures();

		if (!_depthNormalCam) {
			_depthNormalCam = new GameObject("DepthCamera");
			_depthNormalCam.AddComponent<Camera>();
			//_depthNormalCam.camera.enabled = false;
			_depthNormalCam.hideFlags = HideFlags.HideAndDontSave;
			_depthNormalCam.transform.position = new Vector3(13.16343f, 8.719589f, -9.876425f);
			_depthNormalCam.transform.Rotate(24.03025f, 330.3067f, 354.5314f);
		}

		_depthNormalCam.camera.orthographic = true;
		_depthNormalCam.camera.orthographicSize = 6.8f;
		_depthNormalCam.camera.nearClipPlane = 5.74f;
		_depthNormalCam.camera.farClipPlane = 20.25f;

		_depthNormalTex = RenderTexture.GetTemporary((int)_depthNormalCam.camera.pixelWidth, (int)_depthNormalCam.camera.pixelHeight, 32, RenderTextureFormat.Depth);

		//_depthNormalCam.camera.CopyFrom(camera);
		_depthNormalCam.camera.backgroundColor = new Color(0,0,0,0);
		_depthNormalCam.camera.clearFlags = CameraClearFlags.SolidColor;
		//_depthNormalCam.camera.cullingMask = 1 << LayerMask.NameToLayer("Character1") | 1 << LayerMask.NameToLayer("Character2");
		_depthNormalCam.camera.targetTexture = _outputTexture;
		_depthNormalCam.camera.RenderWithShader(
			Shader.Find("Hidden/Camera-DepthNormalTexture"), null);
		_depthNormalCam.camera.depthTextureMode |= DepthTextureMode.DepthNormals;
	}
	
	void OnRenderImage (RenderTexture source, RenderTexture destination) {
		//mat.SetTexture("_DepthNormal", _depthNormalTex);
		mat.SetTexture ("_MainTex", _depthNormalTex);
		//ImageEffects.BlitWithMaterial(material, source, destination);
		CleanUpTextures();
	}		
	
	void OnDisable () {
		if (_depthNormalCam) {
			DestroyImmediate(_depthNormalCam);
		}
		//base.OnDisable();
	}
}
