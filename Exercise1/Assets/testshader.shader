﻿Shader "custom/testshader"
{
	Properties
	{
		_Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_MainTex ("Diffuse Texture", 2D) = "white" {}
		//_BumpMap ("Normal Texture", 2D) = "bump" {}
		_BumpMap ("Normal Texture", 2D) = "white" {}
		//_HeightMap ("Height Map", 2D) = "height" {}
		_HeightMap ("Height Map", 2D) = "white" {}
		_SpecularColor ("Specular Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_Shininess ("Shininess", Float) = 10
		_SelfShadowing ("Self Shadowing", Range(0, 1)) = 0
		_AttenuationMode ("Attenuation Mode", Range(0, 2)) = 2
	}

	SubShader
	{
		//first pass
		Pass {
			Tags {"LightMode" = "ForwardBase"}
			CGPROGRAM
			
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			//base input structs
			struct vertexInput {
				float4 vertex : POSITION;
			};
			
			struct vertexOutput {
				float4 pos : SV_POSITION;
			};
			
			//vertex shader
			vertexOutput vert(vertexInput input) {
				vertexOutput output;
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
			
			//fragment shader
			float4 frag(vertexOutput input) : COLOR {	
				return float4(0.0, 0.0, 0.0, 0.0);
			}
			ENDCG
		}
		
		Pass
		{
			Tags {"LightMode" = "ForwardAdd"}
			Blend One Zero
			CGPROGRAM
			
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 4.0
			
			//includes
			#include "UnityCG.cginc"
			
			//user defined variables
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform sampler2D _BumpMap;
			uniform float4 _BumpMap_ST;
			uniform sampler2D _HeightMap;
			uniform float4 _HeightMap_ST;
			uniform float4 _Color;
			uniform float4 _SpecularColor;
			uniform float _Shininess;
			uniform float _SelfShadowing;
			uniform float _AttenuationMode;
			
			//unity defined variables
			uniform float4 _LightColor0;
			
			//base input structs
			struct vertexInput
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texCoord : TEXCOORD0;
				float4 tangent : TANGENT;
			};
			
			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
				float3 normalWorld : TEXCOORD2;
				float3 tangentWorld : TEXCOORD3;
				float3 binormalWorld : TEXCOORD4;
				float3 camDirTangent : TEXCOORD5;
				float3 lightDirTangent : TEXCOORD6;
				float3 lightDir : TEXCOORD7;
			};
			
			//helper functions
			float2 TraceRay(float height, float2 coords, float3 dir)
			{
				float2 newCoords = coords;
				float2 dUV = -dir.xy * height * 0.08;
				float searchHeight = 1.0;
			    float prev_hits = 0.0;
			    float hit_h = 0.0;

			    for(int ita = 0; ita < 10; ++ita)
			    {
			        searchHeight -= 0.1;
			        newCoords += dUV;
			        float currentHeight = tex2D(_HeightMap, newCoords.xy).r;
			        float first_hit = clamp((currentHeight - searchHeight - prev_hits) * 499999.0,0.0,1.0);
			        hit_h += first_hit * searchHeight;
			        prev_hits += first_hit;
			    }
			    
    			newCoords = coords + dUV * (1.0-hit_h) * 10.0 - dUV;
				float2 temp = newCoords;
    			searchHeight = hit_h + 0.1;
    			float start = searchHeight;
    			dUV *= 0.2;
    			prev_hits = 0.0;
    			hit_h = 0.0;
    			
    			for(int itb = 0; itb < 5; ++itb)
    			{
			        searchHeight -= 0.02;
			        newCoords += dUV;
			        float currentHeight = tex2D(_HeightMap, newCoords.xy * _HeightMap_ST.xy + _HeightMap_ST.zw).r;
			        float first_hit = clamp((currentHeight - searchHeight - prev_hits) * 499999.0,0.0,1.0);
			        hit_h += first_hit * searchHeight;
			        prev_hits += first_hit;    
			    }
    
    			newCoords = temp + dUV * (start - hit_h) * 50.0f;

    			return newCoords;
			}

			//vertex function
			vertexOutput vert(vertexInput v)
			{
				vertexOutput o;
				
				//_World2Object = inverse model matrix
				//_Object2World = model matrix
				o.normalWorld = normalize(mul(float4(v.normal, 0.0), _World2Object).xyz);
				//o.normalWorld = normalize( mul( _Object2World, float4( v.normal, 0.0 )).xyz );
				o.tangentWorld = normalize(v.tangent);
				o.binormalWorld = normalize(cross(o.normalWorld, o.tangentWorld) * v.tangent.w);
				
				o.posWorld = mul(_World2Object, v.vertex);
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.tex = v.texCoord;
				
				float3 viewDirection = _WorldSpaceCameraPos.xyz - o.posWorld.xyz;
   				float3 lightDirection = _WorldSpaceLightPos0.xyz;
   				o.lightDir = _WorldSpaceLightPos0.xyz;

   				float camTanX = dot(o.tangentWorld, viewDirection);
   				float camTanY = dot(o.binormalWorld, viewDirection);
   				float camTanZ = dot(o.normalWorld, viewDirection);
				o.camDirTangent = float3(camTanX, camTanY, camTanZ);
				
				float lightTanX = dot(o.tangentWorld, lightDirection);
   				float lightTanY = dot(o.binormalWorld, lightDirection);
   				float lightTanZ = dot(o.normalWorld, lightDirection);
				o.lightDirTangent = float3(lightTanX, lightTanY, lightTanZ);
				
				return o;
			};
			
			//fragment functuion
			float4 frag(vertexOutput i) : COLOR
			{
				float3 viewDirection = normalize(i.camDirTangent);
				float3 viewDirectionWorld = _WorldSpaceCameraPos.xyz - i.posWorld.xyz;
				//i.normalWorld = normalize(i.normalWorld);
				//float3 normalDirection = i.normalWorld;
				float3 lightDirection;
				float attenuation;
				
				//if (_WorldSpaceLightPos0.w == 0.0)
				//{
				//	lightDirection = normalize(i.lightDirTangent);
				//	attenuation = 1.0;
				//}
				//else
				//{
				
				//NICHT i.lightDirTangent nehmen hier! Deswegen war die Beleuchtung verdreht.
					float3 fragToLightSource = i.lightDir - i.posWorld.xyz;
					float dist = length(fragToLightSource);
					
					//if (_AttenuationMode == 2)
					//{
						attenuation = saturate(1-(1.0/pow(dist,2)));
					//}
					//else if (_AttenuationMode == 1)
					//{
						//float sl = 0.7;
						//float sq = 0.7;
						//attenuation = 1.0/(dist + sl*dist + sq * pow(dist,2));
					//}
					//else
					//{
						//attenuation = 1/dist;
					//}

					lightDirection = normalize(fragToLightSource);
				//}
				
				//float mipmap = 0;
				//float2 ddxVal = ddx(i.Tex);
				//float2 ddyVal = ddy(i.Tex)
				
				//float2 newCoord = TraceRay(0.04, i.tex.xy, viewDirection);
				float2 newCoord = i.tex.xy;
				
				float3 bumpMapNormal = tex2D(_BumpMap, newCoord.xy * _BumpMap_ST.xy + _BumpMap_ST.zw).xyz;
   				bumpMapNormal = normalize(2.0 * bumpMapNormal - float3(1.0, 1.0, 1.0));
				
				float3x3 tex2WorldTranspose = float3x3(
					i.tangentWorld,
					i.binormalWorld,
					i.normalWorld
				);
				float3 vNormal = normalize( mul( (bumpMapNormal), tex2WorldTranspose ) );
				
   				//float3  vNormal = bumpMapNormal;
   				//float nL = dot(vNormal, lightDirection); 

   				//float3  reflection = normalize( ( ( 2.0 * vNormal ) * nL ) - lightDirection ); 
				//float rV = max( 0.0, dot( reflection, viewDirection ) );

				float3 totalDiffuse =  attenuation * _LightColor0.rgb * max(0.0, dot(vNormal, lightDirection) );
				float3 totalSpecular = 0 * attenuation * _LightColor0.rgb * _SpecularColor.rgb * max(0.0, dot(vNormal, lightDirection) ) * pow( max( 0.0, dot( reflect(-lightDirection, vNormal), viewDirectionWorld	 ) ), _Shininess );	
				
				float4  baseColor = tex2D( _MainTex, newCoord.xy * _MainTex_ST.xy + _MainTex_ST.zw);
				//float3  totalAmbient = UNITY_LIGHTMODEL_AMBIENT.rgb * baseColor.rgb; 
				//float3  totalDiffuse = attenuation * nL * baseColor.rgb; 
				//float3  totalSpecular = _SpecularColor.rgb * ( pow( rV, _Shininess ) );
				
				

				float3 lightFinal = (UNITY_LIGHTMODEL_AMBIENT.rgb + (totalDiffuse + totalSpecular));
				
				return float4(baseColor.rgb * lightFinal, 1.0);
			};
			
			ENDCG
		}
	}
	
	//Fallback "Diffuse"
}
